from django.urls import path

from . import views

app_name = "polls"
urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("<int:pk>/results/", views.ResultsView.as_view(), name="results"),
    path("<int:question_id>/comments/", views.CommentView.as_view(), name="comments"),
    path("create/", views.CreatePollView.as_view(), name="create"),
    path("<int:question_id>/edit/", views.EditPollView.as_view(), name="edit"),
    path("history/", views.HistoryView.as_view(), name="history"),
    path("<int:question_id>/vote/", views.vote, name="vote"),
]

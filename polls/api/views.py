from django.db.models import Count
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import generics, permissions, viewsets
from rest_framework.decorators import action, api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from polls.models import Category, Question, Vote

from .permissions import IsAuthorOrReadOnly, ResultPermission
from .serializers import (CategorySerializer, PollResultSerializer,
                          QuestionSerializer, VoteSerializer)


class PollViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.default_api_list_query()
    serializer_class = QuestionSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsAuthorOrReadOnly]

    @action(
        detail=True,
        methods=["get"],
        permission_classes=[permissions.IsAuthenticated, ResultPermission],
    )
    def result(self, request, *args, **kwargs):
        serializer = PollResultSerializer(
            self.get_object(), context={"request": request}
        )
        return Response(serializer.data)


class VoteViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = VoteSerializer

    def get_queryset(self):
        return Vote.objects.filter(voter=self.request.user).order_by("-updated_at")


class CategoryViewApi(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    """ API response will be cached for 1 minute """

    @method_decorator(cache_page(timeout=60))
    def dispatch(self, *args, **kwargs):
        return super(CategoryViewApi, self).dispatch(*args, **kwargs)


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "polls": reverse("api:poll-list", request=request, format=format),
            "user-vote": reverse("api:vote-list", request=request, format=format),
            "categories_list": reverse(
                "api:categories-list", request=request, format=format
            ),
        }
    )

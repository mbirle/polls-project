from django.urls import path
from rest_framework import routers

from . import views

app_name = "api"

router = routers.SimpleRouter()
router.register(r"polls", views.PollViewSet, basename="poll")
router.register(r"votes", views.VoteViewSet, basename="vote")

urlpatterns = [
    path("", views.api_root),
    path("categories/", views.CategoryViewApi.as_view(), name="categories-list"),
]

urlpatterns += router.urls

from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow author of an poll to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.author == request.user


class ResultPermission(permissions.BasePermission):
    """
    Custom permission to only show result to user who voted on poll.
    """

    def has_object_permission(self, request, view, obj):
        return obj.vote_set.filter(voter=request.user).exists()

from django.db import transaction
from django.utils import timezone
from rest_framework import fields, serializers

from polls.models import Category, Choice, Question, Vote
from users.models import User


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ["id", "choice_text"]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "name"]


class QuestionAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "username"]


class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True, source="choice_set")
    author = QuestionAuthorSerializer(read_only=True)
    url = serializers.HyperlinkedIdentityField(view_name="api:poll-detail")
    total_votes = serializers.ReadOnlyField(source="count_total_votes")
    pub_date = serializers.DateTimeField(required=False)

    class Meta:
        model = Question
        fields = [
            "id",
            "url",
            "question_text",
            "category_set",
            "choices",
            "author",
            "total_votes",
            "pub_date",
        ]

    @transaction.atomic
    def create(self, validated_data):
        choice_set = validated_data.pop("choice_set")
        category_set = validated_data.pop("category_set")
        validated_data["pub_date"] = validated_data.get("pub_date") or timezone.now()
        instance = Question.objects.create(
            author=self.context.get("request").user, **validated_data
        )
        choices_list = [
            Choice(question=instance, choice_text=c["choice_text"]) for c in choice_set
        ]
        choices = Choice.objects.bulk_create(choices_list)
        instance.category_set.add(*category_set)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        choice_set = (
            validated_data.pop("choice_set") if "choice_set" in validated_data else None
        )
        choices = self.initial_data.get("choices")
        category_set = (
            validated_data.pop("category_set")
            if "category_set" in validated_data
            else None
        )
        instance = super().update(instance, validated_data)
        if category_set:
            instance.category_set.clear()
            instance.category_set.add(*category_set)
        if choices:
            for c in choices:
                choice = instance.choice_set.get(id=c["id"])
                choice.choice_text = c["choice_text"]
                choice.save()
        return instance


class VoteSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(read_only=True)
    choice = ChoiceSerializer(read_only=True)
    question_id = serializers.PrimaryKeyRelatedField(
        source="question", queryset=Question.objects.all(), write_only=True
    )
    choice_id = serializers.PrimaryKeyRelatedField(
        source="choice", queryset=Choice.objects.all(), write_only=True
    )

    class Meta:
        model = Vote
        fields = ["id", "question", "choice", "question_id", "choice_id"]

    @transaction.atomic
    def create(self, validated_data):
        user = self.context.get("request").user
        if validated_data["question"] == validated_data["choice"].question:
            vote = user.vote_set.create(**validated_data)
            return vote
        raise serializers.ValidationError({"error": "Data Invalid"})

    @transaction.atomic
    def update(self, instance, validated_data):
        if validated_data["question"] == validated_data["choice"].question:
            instance.choice = validated_data["choice"]
            instance.save()
            return instance
        raise serializers.ValidationError({"error": "Data Invalid"})


class ChoiceResultSerializer(ChoiceSerializer):
    total_votes = serializers.ReadOnlyField(source="count_total_votes")

    class Meta:
        model = Choice
        fields = ChoiceSerializer.Meta.fields + ["total_votes"]


class PollResultSerializer(QuestionSerializer):
    choices = ChoiceResultSerializer(many=True, source="choice_set")

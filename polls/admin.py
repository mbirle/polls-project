from django.contrib import admin

from .models import Category, Choice, Question


class ChoiceInline(admin.TabularInline):
    model = Choice

    def get_extra(self, request, obj=None, **kwargs):
        return 0 if (obj is not None and obj.choice_set.count() > 0) else 3


class CategoryInline(admin.TabularInline):
    model = Question.category_set.through

    def get_extra(self, request, obj=None, **kwargs):
        return 0 if (obj is not None and obj.choice_set.count() > 0) else 3


class QuestionAdmin(admin.ModelAdmin):
    # fields = ['pub_date', 'question_text']
    fieldsets = [
        ("Author", {"fields": ["author"]}),
        ("Question", {"fields": ["question_text"]}),
        ("Date information", {"fields": ["pub_date"]}),
    ]
    inlines = [CategoryInline, ChoiceInline]
    list_display = (
        "question_text",
        "author",
        "pub_date",
        "choice_count",
        "was_published_recently",
    )
    list_filter = ["pub_date"]
    search_fields = ["question_text", "author__username"]


class CategoryAdmin(admin.ModelAdmin):
    fields = ["name"]
    list_display = ("name", "total_questions")
    search_fields = ["name"]


admin.site.register(Question, QuestionAdmin)
admin.site.register(Category, CategoryAdmin)

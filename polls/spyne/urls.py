from django.conf.urls import url

from . import views

urlpatterns = [
    url("question_interface/", views.question_interface),
]

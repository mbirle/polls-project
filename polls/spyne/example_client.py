from suds.client import Client

url = "http://127.0.0.1:8000/spyne/question_interface/?wsdl"
client = Client(url)

print(client)


print("\n------- Get Question List --------\n")
response = client.service.getQuestionList()
print(str(response))


print("\n------- Create a Question --------\n")
question = client.factory.create("ns1:QuestionModel")
question.author_id = "1"
question.question_text = "This question is generated thorough SOAP API."
question.pub_date = "2021-07-28T19:00:00"
response = client.service.createQuestion(question)
print(str(response))


print("\n------- Fetch the Question --------\n")
question_id = response.id
response = client.service.fetchQuestion(question_id)
print(str(response))


print("\n------- Update the Question --------\n")
question = client.factory.create("ns1:QuestionModel")
question.id = question_id
question.question_text = "This question is updated thorough SOAP API."
question.pub_date = "2021-07-28T19:00:00"
response = client.service.updateQuestion(question)
print(str(response))


print("\n------- Delete the Question --------\n")
response = client.service.deleteQuestion(question_id)
print(str(response))

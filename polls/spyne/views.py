from django.views.decorators.csrf import csrf_exempt
from spyne import Application, ServiceBase, Unicode, rpc
from spyne.model.complex import Array, ComplexModel, Iterable
from spyne.model.primitive import AnyDict, DateTime, Integer
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication

from polls.api.serializers import QuestionSerializer
from polls.models import Category, Choice, Question


class QuestionModel(ComplexModel):
    id = Integer
    author_id = Integer
    question_text = Unicode
    pub_date = DateTime

    @classmethod
    def init_from_db_record(cls, model):
        obj = cls()
        obj.id = model.id
        obj.author_id = model.author_id
        obj.question_text = model.question_text
        obj.pub_date = model.pub_date
        return obj


class QuestionInterface(ServiceBase):
    @rpc(_returns=Iterable(QuestionModel))
    def getQuestionList(self):
        question = Question.objects.all()
        for q in question:
            yield QuestionModel.init_from_db_record(q)

    @rpc(Integer, _returns=QuestionModel)
    def fetchQuestion(self, id):
        question = Question.objects.get(id=id)
        return QuestionModel.init_from_db_record(question)

    @rpc(QuestionModel, _returns=QuestionModel)
    def createQuestion(self, question_data):
        que = Question()
        que.author_id = question_data.author_id
        que.question_text = question_data.question_text
        que.pub_date = question_data.pub_date
        que.save()
        return QuestionModel.init_from_db_record(que)

    @rpc(QuestionModel, _returns=QuestionModel)
    def updateQuestion(self, question_data):
        que = Question.objects.get(id=question_data.id)
        que.question_text = question_data.question_text
        que.pub_date = question_data.pub_date
        que.save()
        return QuestionModel.init_from_db_record(que)

    @rpc(Integer, _returns=Unicode)
    def deleteQuestion(self, id):
        question = Question.objects.get(id=id)
        question.delete()
        return "Deleted Object"


application = Application(
    [QuestionInterface],
    tns="http://schemas.xmlsoap.org/soap/envelope/",
    in_protocol=Soap11(),
    out_protocol=Soap11(),
)

question_interface = csrf_exempt(DjangoApplication(application))

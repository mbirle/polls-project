from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Vote


@receiver(post_save, sender=Vote)
def push_update_vote_count(sender, instance, created, **kwargs):
    if not created:
        return
    question_id = instance.question_id
    data = {"vote_count": instance.question.count_total_votes()}
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "question_%s" % (question_id),
        {"type": "question_push_updates", "message": data},
    )

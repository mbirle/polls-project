import datetime

from django.contrib import admin
from django.db import models
from django.utils import timezone

from users.models import User
from .managers import QuestionQuerySet


class Question(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, default="", null=False, blank=False
    )
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published")
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    objects = QuestionQuerySet.as_manager()

    @admin.display(
        boolean=True,
        ordering="pub_date",
        description="Published recently?",
    )
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    def choice_count(self):
        return self.choice_set.count()

    def count_total_votes(self):
        if hasattr(self, "vote__count"):
            return self.vote__count
        return self.vote_set.count()

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def count_total_votes(self):
        return self.vote_set.count()

    def __str__(self):
        return self.choice_text


class Vote(models.Model):
    voter = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (("voter", "question"),)


class Category(models.Model):
    questions = models.ManyToManyField(Question)
    name = models.CharField(max_length=100)

    def total_questions(self):
        return self.questions.count()

    def __str__(self):
        return self.name

    @classmethod
    def get_list(cls):
        return [[c.id, c.name] for c in cls.objects.all().only("id", "name")]


class Comment(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    commenter = models.ForeignKey(User, on_delete=models.CASCADE)
    comment_text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comment_text

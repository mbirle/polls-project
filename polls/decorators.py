from django.shortcuts import get_object_or_404, redirect, render

from .models import Question


def set_question_and_verify_author(func):
    def wrap(request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        if request.user == question.author:
            return func(request, question_id, question=question)
        else:
            return redirect("polls:detail", question.id)

    return wrap

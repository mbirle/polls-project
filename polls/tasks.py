from celery import Celery
from celery.schedules import crontab
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone

from .models import Question, User

app = Celery()


@app.task
def new_polls_notification_job():
    new_polls_available = Question.objects.filter(
        pub_date__gte=timezone.now() - timezone.timedelta(hours=1)
    ).exists()
    if new_polls_available:
        recipient_list = [u["email"] for u in User.objects.all().values("email")]
        email_from = settings.EMAIL_HOST_USER
        subject = "New Polls Available"
        message = "Hello, new polls are now available on http://localhost:8000"
        send_mail(subject, message, email_from, recipient_list)

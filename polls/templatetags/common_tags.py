from django.template import Library, Node
from django.utils.safestring import mark_safe

register = Library()


@register.simple_tag
def print_question_category(question):
    result_html = ""
    for category in question.category_set.all():
        result_html += " <span class='badge badge-primary'>%s</span> " % (category.name)
    return mark_safe(result_html)

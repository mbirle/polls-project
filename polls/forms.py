from django import forms
from django.utils import timezone

from .models import Category, Choice, Comment, Question, Vote


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ["question_text", "category"]

    question_text = forms.CharField(label="Question/Topic")

    category = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )

    def save(self, commit=True, user=None):
        question = super(QuestionForm, self).save(commit=False)
        question.pub_date = timezone.now()
        question.author = user
        if question:
            question.save()
        return question


class VoteForm(forms.ModelForm):
    class Meta:
        model = Vote
        fields = ["choices"]

    choices = forms.ChoiceField(
        widget=forms.RadioSelect, choices=[], label="Vote Your Choice", required=True
    )

    def __init__(self, *args, **kwargs):
        self.question = kwargs["initial"]["question"]
        self.user = kwargs["initial"]["user"]
        super(VoteForm, self).__init__(*args, **kwargs)
        choice_list = [
            (choice.id, choice.choice_text) for choice in self.question.choice_set.all()
        ]
        self.fields["choices"].choices = choice_list
        if self.user.is_authenticated:
            vote = self.user.vote_set.filter(question=self.question).first()
            selected_choice_id = vote.choice_id if vote is not None else None
            self.fields["choices"].initial = selected_choice_id


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["comment_text"]

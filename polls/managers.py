from django.db import models
from django.db.models import Count
from django.utils import timezone


class QuestionQuerySet(models.QuerySet):
    def published(self):
        return self.filter(pub_date__lte=timezone.now())

    def prefetch_category(self):
        return self.prefetch_related("category_set")

    def prefetch_choices(self):
        return self.prefetch_related("choice_set")

    def prefetch_author(self):
        return self.select_related("author")

    def prefetch_vote_count(self):
        return self.annotate(Count("vote"))

    def order_by_published(self):
        return self.order_by("-pub_date")

    def default_list_query(self):
        return (
            self.published()
            .prefetch_category()
            .prefetch_vote_count()
            .order_by_published()
        )

    def default_api_list_query(self):
        return self.default_list_query().prefetch_choices().prefetch_author()

    def filter_by_category(self, category_id):
        return self.filter(category__id=category_id)

    def filter_user_voted_question(self, user_id):
        return self.filter(vote__voter_id=user_id)

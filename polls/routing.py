from django.urls import path, re_path

from . import consumers

websocket_urlpatterns = [
    path("ws/question/<int:question_id>/", consumers.QuestionConsumer.as_asgi()),
]

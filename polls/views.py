from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import Count
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View, generic
from django.views.generic.edit import FormMixin

from .decorators import set_question_and_verify_author
from .forms import CommentForm, QuestionForm, VoteForm
from .models import Category, Choice, Comment, Question


class IndexView(generic.ListView):
    template_name = "polls/index.html"

    def get_queryset(self):
        """Return the last five published questions."""
        queryset = Question.objects.default_list_query()
        
        category_id = self.request.GET.get("category")
        if category_id:
            queryset = queryset.filter_by_category(category_id)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        extra_context = {
            "category_list_data": Category.get_list(),
        }
        context.update(extra_context)
        return context


class DetailView(FormMixin, generic.DetailView):
    model = Question
    template_name = "polls/detail.html"
    form_class = VoteForm

    # This will pass to the form_class on initalization
    def get_initial(self):
        return {"question": self.get_object(), "user": self.request.user}


class ResultsView(generic.DetailView):
    model = Question
    template_name = "polls/results.html"


class HistoryView(LoginRequiredMixin, generic.ListView):
    template_name = "polls/history.html"
    login_url = "user:login"

    def get_queryset(self):
        """Return the list of polls in which user have participated"""
        return Question.objects.default_list_query().filter_user_voted_question(self.request.user.id)


class CreatePollView(LoginRequiredMixin, View):
    login_url = "user:login"

    def get(self, request):
        question_form = QuestionForm()
        choice_form = inlineformset_factory(
            Question, Choice, fields=("choice_text",), extra=4
        )
        context = {
            "question_form": question_form,
            "choice_form": choice_form,
            "action": "Create",
        }
        return render(request, "polls/poll_form.html", context)

    def post(self, request):
        question_form = QuestionForm(request.POST)
        choice_form = inlineformset_factory(
            Question, Choice, fields=("choice_text",), extra=4
        )
        if question_form.is_valid():
            with transaction.atomic():
                question = question_form.save(user=request.user)
                question.category_set.add(*question_form.cleaned_data["category"])
                choice_form = choice_form(request.POST, instance=question)
                assert choice_form.is_valid(), "Form Invalid!"
                choices = choice_form.save()
            messages.success(self.request, "Poll Created")
            return redirect("polls:detail", question.id)
        else:
            messages.error(self.request, "Error Occured")
            context = {
                "question_form": question_form,
                "choice_form": choice_form,
                "action": "Create",
            }
            return render(request, "polls/poll_form.html", context)


class EditPollView(LoginRequiredMixin, View):
    login_url = "user:login"

    @method_decorator(set_question_and_verify_author)
    def get(self, request, question_id, question=None):
        question_form = QuestionForm(
            instance=question, initial={"category": question.category_set.all()}
        )
        ChoiceForm = inlineformset_factory(
            Question, Choice, fields=("choice_text",), extra=0
        )
        choice_form = ChoiceForm(instance=question)
        context = {
            "question_form": question_form,
            "choice_form": choice_form,
            "action": "Update",
        }
        return render(request, "polls/poll_form.html", context)

    @method_decorator(set_question_and_verify_author)
    def post(self, request, question_id, question=None):
        question_form = QuestionForm(request.POST, instance=question)
        ChoiceForm = inlineformset_factory(Question, Choice, fields=("choice_text",))
        choice_form = ChoiceForm(request.POST, instance=question)
        if question_form.is_valid() and choice_form.is_valid():
            with transaction.atomic():
                question = question_form.save(user=request.user)
                choices = choice_form.save()
                question.category_set.clear()
                question.category_set.add(*question_form.cleaned_data["category"])
            messages.success(self.request, "Poll Updated")
            return redirect("polls:detail", question.id)
        else:
            messages.error(self.request, "Error Occured")
            context = {
                "question_form": question_form,
                "choice_form": choice_form,
                "action": "Update",
            }
            return render(request, "polls/poll_form.html", context)


class CommentView(generic.CreateView):
    form_class = CommentForm
    template_name = "polls/comment_list.html"

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.commenter_id = self.request.user.id
        obj.question_id = self.kwargs.get("question_id")
        obj.save()
        messages.success(self.request, "Comment added")
        return redirect("polls:detail", self.kwargs.get("question_id"))

    def get_context_data(self, **kwargs):
        context = super(CommentView, self).get_context_data(**kwargs)
        extra_context = {
            "comment_list": Comment.objects.filter(
                question_id=self.kwargs.get("question_id")
            )
            .select_related("commenter")
            .order_by("created_at")
            .only("id", "comment_text", "created_at", "commenter__username"),
            "question_id": self.kwargs.get("question_id"),
        }
        context.update(extra_context)
        return context


@login_required(login_url="user:login", redirect_field_name=None)
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    vote_form = VoteForm(
        request.POST, initial={"question": question, "user": request.user}
    )
    if vote_form.is_valid():
        vote, created = request.user.vote_set.update_or_create(
            question=question, defaults={"choice_id": vote_form.cleaned_data["choices"]}
        )
        messages.success(request, "Successfully Voted" if created else "Vote Updated")
    else:
        messages.error(request, "Something wend wrong.")
    return redirect("polls:results", question.id)

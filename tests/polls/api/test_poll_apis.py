import json

import pytest
from django.urls import reverse
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken


@pytest.mark.django_db
def test_poll_list_api(api_client, random_question_factory):
    random_question_factory.create_batch(20)

    expected_status = 200
    expected_count = 20

    url = reverse("api:poll-list")
    response = api_client.get(url)

    status = response.status_code
    content = json.loads(response.content)

    print(response, content)

    assert status == expected_status
    assert content["count"] == expected_count


@pytest.mark.django_db
def test_poll_retrieve_api(api_client, random_question_factory):
    poll = random_question_factory.create()

    expected_status = 200

    url = reverse("api:poll-detail", kwargs={"pk": poll.id})
    response = api_client.get(url)

    status = response.status_code
    content = json.loads(response.content)

    print(response, content)

    assert status == expected_status
    assert content["id"] == poll.id


@pytest.mark.django_db
def test_poll_create_api(api_client, random_user_factory):
    expected_status = 201

    url = reverse("api:poll-list")
    user = random_user_factory.create()
    auth_api_client(api_client, user)

    post_data = {
        "question_text": "New Question For Testing APIs",
        "category_set": [],
        "choices": [
            {"choice_text": "Choice 1"},
            {"choice_text": "Choice 2"},
            {"choice_text": "Choice 3"},
            {"choice_text": "Choice 4"},
        ],
    }

    response = api_client.post(url, data=post_data, format="json")

    status = response.status_code
    content = json.loads(response.content)

    print(response, content)

    assert status == expected_status
    assert content["question_text"] == post_data["question_text"]
    assert content["author"]["username"] == user.username


@pytest.mark.django_db
def test_poll_put_api(api_client, random_user_factory, random_question_factory):
    expected_status = 200

    user = random_user_factory.create()
    poll = random_question_factory.create(author=user)
    auth_api_client(api_client, user)
    url = reverse("api:poll-detail", kwargs={"pk": poll.id})

    update_data = {
        "question_text": "Update Question Text For Testing APIs",
        "category_set": [],
        "choices": [
            {"id": c.id, "choice_text": ("Updated" + c.choice_text)}
            for c in poll.choice_set.all()
        ],
    }
    response = api_client.put(url, data=update_data, format="json")
    status = response.status_code
    content = json.loads(response.content)

    print(response, content)

    assert status == expected_status
    assert content["question_text"] == update_data["question_text"]
    assert content["question_text"] != poll.question_text


@pytest.mark.django_db
def test_poll_patch_api(api_client, random_user_factory, random_question_factory):
    expected_status = 200

    user = random_user_factory.create()
    poll = random_question_factory.create(author=user)
    auth_api_client(api_client, user)
    url = reverse("api:poll-detail", kwargs={"pk": poll.id})

    update_data = {"question_text": "Only Updating Question Text For Testing APIs"}
    response = api_client.patch(url, data=update_data, format="json")
    status = response.status_code
    content = json.loads(response.content)

    print(response, content)

    assert status == expected_status
    assert content["question_text"] == update_data["question_text"]
    assert content["question_text"] != poll.question_text


@pytest.mark.django_db
def test_poll_delete_api(api_client, random_user_factory, random_question_factory):
    expected_status = 204
    user = random_user_factory.create()
    poll = random_question_factory.create(author=user)
    auth_api_client(api_client, user)
    url = reverse("api:poll-detail", kwargs={"pk": poll.id})

    response = api_client.delete(url)

    status = response.status_code

    print(response)

    assert status == expected_status


@pytest.mark.django_db
def test_poll_result_api(api_client, random_user_factory, random_question_factory):
    user = random_user_factory.create()
    poll = random_question_factory.create()
    auth_api_client(api_client, user)
    url = reverse("api:poll-result", kwargs={"pk": poll.id})

    response = api_client.get(url)
    content = json.loads(response.content)
    print(response, content)
    # Forbideen user to now show result if he didn't voted on poll yet.
    assert response.status_code == 403

    # Create a vote on poll for user
    vote = poll.vote_set.create(voter=user, choice_id=poll.choice_set.all()[0].id)

    response = api_client.get(url)
    content = json.loads(response.content)
    print(response, content)
    assert response.status_code == 200


def auth_api_client(api_client, user):
    token = AccessToken.for_user(user)
    api_client.credentials(HTTP_AUTHORIZATION=f"Bearer {token}")

from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail

from .models import User


@shared_task
def send_registration_success_email(user_id):
    user = User.objects.filter(id=user_id)[0]
    if user:
        subject = "Registraion Success"
        message = "Thank you %s for registering to our site" % user.username
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [
            user.email,
        ]
        send_mail(subject, message, email_from, recipient_list)

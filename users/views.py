from django.contrib import messages
from django.contrib.auth import login
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.views import View, generic

from .forms import RegisterUserForm
from .mailer import send_registration_success_email
from .models import User


class RegisterUserView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect("polls:index")
        form = RegisterUserForm()
        return render(request, "registration/register.html", {"form": form})

    def post(self, request):
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            send_registration_success_email.delay(user.id)
            messages.success(request, "Registration successful.")
            return redirect("polls:index")
        messages.error(request, "Unsuccessful registration. Invalid information.")
        return render(request, "registration/register.html", {"form": form})

Poetry Setup Instruction:

1. Install poetry on system with (Ignore if already install):
    `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python3`

2. Run `poetry install` command from project root directory to install dependencies.

3. We can add new packages with `poetry add <package_name>`.

4. We can now run our commands with `poetry run`.
    For example `poetry run python manage.py runserver` to start server.

5. Alternatively, We can switch to poetry venv with command `poetry shell` and then we can run command directly.


Run WSGI server with gunicorn
    - `gunicorn polling_project.wsgi:application`


Run ASGI server with daphne
    - `daphne -p 8001 polling_project.asgi:application`


Note:

1. Also required redis server to be running on localhost:6379

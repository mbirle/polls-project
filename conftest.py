import factory
import factory.fuzzy
import pytest
from django.utils import timezone
from pytest_factoryboy import register
from rest_framework.test import APIClient

from polls.models import Choice, Question
from users.models import User


@pytest.fixture
def api_client():
    return APIClient()


class RandomUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    username = factory.LazyAttribute(
        lambda a: "{}.{}".format(a.first_name, a.last_name).lower()
    )
    email = factory.LazyAttribute(
        lambda a: "{}.@example.com".format(a.username).lower()
    )
    password = "Fake@123321"


class ChoiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Choice

    choice_text = factory.fuzzy.FuzzyText(length=100)


class RandomQuestionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Question

    question_text = factory.Faker("text")
    author = factory.SubFactory(RandomUserFactory)
    pub_date = timezone.now()

    @factory.post_generation
    def generat_choices(self, create, extracted, **kwargs):
        return ChoiceFactory.create_batch(4, question=self)


register(RandomUserFactory)
register(RandomQuestionFactory)
